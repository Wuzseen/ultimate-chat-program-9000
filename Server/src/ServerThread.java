import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Calendar;


public class ServerThread implements Runnable{
	protected DatagramSocket socket, sock2;
	protected boolean haveMessage;
	protected String name, fname;
	protected BufferedWriter logger;
	
	public ServerThread() throws IOException {
		this("ServerThread");
	}
	
	private void createLog() throws IOException {
		Calendar calendar = Calendar.getInstance(); 
		int mon = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int year = calendar.get(Calendar.YEAR);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int min = calendar.get(Calendar.MINUTE);
		int sec = calendar.get(Calendar.SECOND);
		fname = "logs/" + mon + "_" + day + "_" + year + "_" + hour + "_" + min + "_" + sec + ".log";
		File f = new File(fname);
		System.out.println("Log file created:\n" + fname + "\n");
		logger = new BufferedWriter(new FileWriter(f.getAbsoluteFile()));
		logger.write("New chat log for session started at:\n" + fname + "\n");
		logger.close();
	}

	private void log(ChatProtocol protocol) throws IOException {
		File f = new File(fname);
		logger = new BufferedWriter(new FileWriter(f.getAbsoluteFile(),true));
		Calendar calendar = Calendar.getInstance(); 
		logger.write(calendar.get(Calendar.HOUR) + ":" + calendar.get(Calendar.MINUTE) + "> " + protocol.toString() + "\n");
	}
	
	public ServerThread(String name) throws IOException {
		this.name = name;
		haveMessage = true;
		socket = new DatagramSocket(4083);
		System.out.println("Server created, listening on port 4083.");
		System.out.println("Sending to inet grp 239.0.0.8");
		createLog();
		// Might do something else here, but should be it for now
	}
	
	@Override
	public void run() {
		byte[] buf = new byte[2048 + 17640];
		
		DatagramPacket packet = new DatagramPacket(buf, buf.length);
		while(true) {
			try {
				socket.receive(packet);
				ChatProtocol message = (ChatProtocol)ChatProtocol.deserialize(packet.getData());
				if(!message.containsAudio)
					System.out.println("SERVER LOG: " + message.toString());
				
				// Send something back to the socket intended for recipient
				// If recipient is null/"" send it to every connected client
				InetAddress ip = InetAddress.getByName("239.0.0.8");
				int port = 4082;
				packet = new DatagramPacket(buf, buf.length, ip, port);
				socket.send(packet);
				if(!message.containsAudio)
					log(message);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				break;
			}
		}
		socket.close();
	}
}
