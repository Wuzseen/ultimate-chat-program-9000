import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

/*
 * ClientInputThread.java
 * Ultimate Chat Program 9000
 * CS283 Summer 2013
 * Brandon Harrison, Tim Day, Brian Tighe
 */

public class ClientAudioInput implements Runnable{
	protected DatagramSocket skt;
	protected ChatProtocol chat;
	protected String msg;
	private boolean useMic = true;
	private String host;
	
	public ClientAudioInput(String hoststr) throws SocketException {
		skt = new DatagramSocket();
		host = hoststr;
	}
	
	@Override
	public void run() {
		try {
			AudioFormat	audioFormat = new AudioFormat(
					AudioFormat.Encoding.PCM_SIGNED,
					44100.0F, 16, 2, 4, 44100.0F, false);
			DataLine.Info	infotarget = new DataLine.Info(TargetDataLine.class, audioFormat);
			TargetDataLine	microphone = null;

			try {
				if(useMic) {
					microphone = (TargetDataLine) AudioSystem.getLine(infotarget);
					microphone.open(audioFormat);
					microphone.start();
				}
			} catch (LineUnavailableException e) {
				// TODO Auto-generated catch block
				useMic = false;
				e.printStackTrace();
			}
			byte[] buf = new byte[2048 + 17640];
			DatagramPacket pkt;
			while(true) {
				if(useMic) {
					chat = new ChatProtocol(Client.user,null,msg);
					chat.containsAudio = true;
					microphone.read(chat.audioData, 0, chat.audioData.length);
					buf = ChatProtocol.serialize(chat);
					pkt = new DatagramPacket(buf, buf.length, InetAddress.getByName(host), 4083);
					skt.send(pkt);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
