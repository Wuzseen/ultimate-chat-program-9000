
import javax.sound.sampled.*;
public class SimpleAudioRecorder extends Thread {
	public static void main(String[] args) {
		AudioFormat	audioFormat = new AudioFormat(
			AudioFormat.Encoding.PCM_SIGNED,
			44100.0F, 16, 2, 4, 44100.0F, false);

		int numBytesRead;		
		
		DataLine.Info	infosource = new DataLine.Info(SourceDataLine.class, audioFormat);
		DataLine.Info	infotarget = new DataLine.Info(TargetDataLine.class, audioFormat);
		TargetDataLine	microphone = null;
		SourceDataLine	speakers = null;
		
		try
		{
			microphone = (TargetDataLine) AudioSystem.getLine(infotarget);
			byte[] data = new byte[microphone.getBufferSize() / 5];
			microphone.open(audioFormat);
			speakers = (SourceDataLine) AudioSystem.getLine(infosource);	
			speakers.open(audioFormat);
			
			microphone.start();
			speakers.start();
			while (true) {
			   numBytesRead =  microphone.read(data, 0, data.length);
			   System.out.println();
			   // Save this chunk of data.
			   speakers.write(data, 0, numBytesRead);
			}     	
		}
		catch (LineUnavailableException e) {
			System.out.println("unable to get a recording line");
			e.printStackTrace();
			System.exit(1);
		}
	}
}