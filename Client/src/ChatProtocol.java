import java.io.*;

// Main chat protocol
@SuppressWarnings("serial")
public class ChatProtocol implements Serializable {
	public String sender, target, message;
	public boolean containsAudio;
	public byte[] audioData;
	
	public ChatProtocol(String send, String tar, String mess) {
		setSender(send);
		setTarget(tar);
		setMessage(mess);
		audioData = new byte[17640];
	}
	
	public void setAudio(byte[] data) {
		audioData = data;
	}
	
	public byte[] getAudio() {
		return audioData;
	}
	
	public void setSender(String send) {
		sender = send;
	}
	
	public void setTarget(String targ) {
		target = targ;
	}
	
	public void setMessage(String mess) {
		message = mess;
	}
	
	public String getSender() {
		return sender;
	}
	
	public String getTarget() {
		return target;
	}
	
	public String getMessage() {
		return message;
	}
	
	public String toString() {
		return sender + ": " + message;
	}
	
	public static byte[] serialize(Object obj) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		ObjectOutputStream os = new ObjectOutputStream(out);
		os.writeObject(obj);
		return out.toByteArray();
	}
	
	public static Object deserialize(byte[] data) throws IOException, ClassNotFoundException {
		ByteArrayInputStream in = new ByteArrayInputStream(data);
		ObjectInputStream is = new ObjectInputStream(in);
		return is.readObject();
	}
}
