import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.util.ArrayList;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;

/*
 * ClientThread.java
 * Ultimate Chat Program 9000
 * CS283 Summer 2013
 * Brandon Harrison, Tim Day, Brian Tighe
 */

public class ClientThread implements Runnable{
	protected MulticastSocket skt;
	protected InetAddress group;
	protected ChatProtocol chat;
	private boolean useSpeakers = true;
	public ArrayList<byte[]> audioSamples;
	
	public ClientThread() throws IOException {
		skt = new MulticastSocket(4082);
		group = InetAddress.getByName("239.0.0.8");
		skt.joinGroup(group);
	}

	@Override
	public void run() {
		try {	
			AudioFormat	audioFormat = new AudioFormat(
					AudioFormat.Encoding.PCM_SIGNED,
					44100.0F, 16, 2, 4, 44100.0F, false);	
			DataLine.Info	infosource = new DataLine.Info(SourceDataLine.class, audioFormat);
			SourceDataLine	speakers = null;
			byte[] buf = new byte[2048 + 17640]; // Part for text, part for audio--it's... it's better this way
			DatagramPacket pktR = new DatagramPacket(buf, buf.length);
			try {
				if(useSpeakers) {
					speakers = (SourceDataLine) AudioSystem.getLine(infosource);
					speakers.open(audioFormat);
					speakers.start();
				}
			} catch (LineUnavailableException e1) {
				e1.printStackTrace();
				System.out.println("No speakers detected.");
				useSpeakers = false;
			}
			while(true) {	
				skt.receive(pktR);
				try {
					chat = (ChatProtocol)ChatProtocol.deserialize(pktR.getData());
					if(chat.containsAudio && !chat.sender.equals(Client.user)) {
						speakers.write(chat.getAudio(), 0, chat.getAudio().length);
					}
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					break;
				}
				if(!chat.containsAudio) {
					if(chat.getTarget().equals(Client.user) && !chat.getSender().equals(Client.user))
						System.out.println("Whisper from " + chat.toString());
					else if(!chat.getSender().equals(Client.user) && chat.getTarget().equals(""))
						System.out.println(chat.toString());
				}
			}
			skt.leaveGroup(group);
			skt.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
