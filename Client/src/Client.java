import java.io.*;
import java.util.*;

/*
 * Client.java
 * Ultimate Chat Program 9000
 * CS283 Summer 2013
 * Brandon Harrison, Tim Day, Brian Tighe
 */

public class Client {
	public static String user;
	public static void main(String[] args) throws IOException {
		String host = args[0];
		String mic = "";
		if(args[1] != null)
			mic = args[1];
		boolean gomic = false;
		if(mic.equals("usemic"))
			gomic = true;
		try {
			Scanner nScan = new Scanner(System.in);
			System.out.println("Enter name.");
			user = nScan.nextLine();
			while(user.equals("") || user.contains(" ")) {
				if(user.contains(" "))
					System.out.println("No spaces in names, please reenter.");
				else
					System.out.println("Reenter name.");
				user = nScan.nextLine();
			}
			System.out.println("Connecting to server.\n");
			Runnable in, out, audio;
			in = new ClientInputThread(host);
			out = new ClientThread();
			audio = new ClientAudioInput(host);
			new Thread(in).start();
			new Thread(out).start();
			if(gomic)
				new Thread(audio).start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}