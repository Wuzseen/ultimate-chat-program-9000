import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Scanner;

/*
 * ClientInputThread.java
 * Ultimate Chat Program 9000
 * CS283 Summer 2013
 * Brandon Harrison, Tim Day, Brian Tighe
 */

public class ClientInputThread implements Runnable{
	protected DatagramSocket skt;
	protected ChatProtocol chat;
	protected String msg, host;
	
	public ClientInputThread(String hoststr) throws SocketException {
		skt = new DatagramSocket();
		host = hoststr;
	}
	
	// Whispers are of format %target msg
	private String getTar(String msg) {
		String tar = "";
		if(msg.charAt(0) == '%') { // % is token for whisper {
			tar = msg.substring(1, msg.indexOf(' '));	
		}
		return tar;
	}
	
	private String getMsg(String tar, String msg) {
		if(tar.equals("")) {
			return msg;
		} else
			return msg.substring(tar.length() + 2);
	}
	
	private boolean checkCommand(String msg) {
		boolean isMsg = false;
		if(msg.charAt(0) == '/')
			isMsg = true;
		if(isMsg) {
			String command = msg.substring(1); 
			if(command.toLowerCase().contains("rename")) {
				Scanner f = new Scanner(System.in);
				System.out.println("Renenter name:");
				String tempuser = f.nextLine();
				while(tempuser.equals("") || tempuser.contains(" ")) {
					if(tempuser.contains(" "))
						System.out.println("No spaces in names, please reenter.");
					else
						System.out.println("Reenter name.");
					tempuser = f.nextLine();
				}
				Client.user = tempuser;
			} else if(command.toLowerCase().contains("help")) {
				System.out.println("ChatMaster 9000 Help Menu ---\n" +
						"Whisper by using \"%target message\" where target is the intended recipient\n" +
						"Rename by typing \"/rename\"\n" +
						"Quit by typing \"/quit\"");
			} else if(command.toLowerCase().contains("quit")) {
				System.out.println("CANNOT QUIT, THE MATRIX HAS YOU");
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				skt.close();
				System.out.println("Just kidding... bye :(");
				System.exit(0);
			} else
				System.out.println("Command not recognized.");
		}
		return isMsg;
	}
	
	@Override
	public void run() {
		try {
			Scanner reader = new Scanner(System.in);
			byte[] buf = new byte[2048 + 17640];
			DatagramPacket pkt;
			System.out.println("Enter messages by typing and then hitting enter (/help for help):");
			while(true) {
				msg = reader.nextLine();
				if(!msg.equals("") && !checkCommand(msg)) {
					String target = getTar(msg);
					msg = getMsg(target,msg);
					chat = new ChatProtocol(Client.user,target,msg);
					chat.containsAudio = false;
					buf = ChatProtocol.serialize(chat);
					pkt = new DatagramPacket(buf, buf.length, InetAddress.getByName(host), 4083);
					skt.send(pkt);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
