buildall :
	javac ./Server/src/*.java
	javac ./Client/src/*.java

server :
	java -cp ./Server/src/ Server

clientmic :
	java -cp ./Client/src/ Client ${HOST} "usemic"

client :
	java -cp ./Client/src/ Client ${HOST} "nomic"

clean :
	rm -f ./Client/src/*.class
	rm -f ./Server/src/*.class
